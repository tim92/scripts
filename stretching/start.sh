#! /usr/bin/env nix-shell
#! nix-shell -i python -p python3 python37Packages.pyyaml espeak mpv
from subprocess import Popen, DEVNULL
from datetime import datetime
from time import sleep
import yaml


pause_between_sets = 30
pause_between_exercises = 15

with open("exercises.yaml", "r") as f:
    exercises = yaml.load(f)

goo = 'Popen(("mpv", "01 Countdown - Go.mp3"), stdout=DEVNULL).wait()'
stp = 'Popen(("mpv", "02 Countdown - Stop.mp3"), stdout=DEVNULL).wait()'
rst = 'Popen(("mpv", "03 Countdown - Rest.mp3"), stdout=DEVNULL).wait()'

def say(sentence, wait_for_bg_proccess=True):
    print(sentence)
    args = ("espeak", sentence)
    bg_process = Popen(args)
    if wait_for_bg_proccess:
        bg_process.wait()

say("Starting stretching.")
sleep(1)

for (ex, i) in zip(exercises, range(len(exercises))):
    s = "Prepare for exercise " + ex['name'] + " with " + str(ex['per set']) + " iterations."
    say(s)
    for j in range(ex['per set']):
        s = "Prepare for iteration " + str(j + 1) + " of " + ex['name'] + "."
        if ex['variation']:
            s += " Do variation " + ex['variation'][j] + "."
        say(s)
        eval(goo)
        sleep(ex['time'] - 5)
        eval(rst)
        if j + 1 < ex['per set']:
            sleep(pause_between_exercises - 5)
    if i + 1 < len(exercises):
        s = "Rest between " + ex['name'] + " and " + exercises[i + 1]['name'] + "."
        say(s)
        sleep(pause_between_sets - 5)
sleep(1)
say("Done stretching.")

with open('/home/tim/Sync/Body/stretching.txt', 'at') as f:
    f.write(str(datetime.now()) + "\n")

